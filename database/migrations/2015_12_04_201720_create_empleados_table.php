<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('empresaremun_id')->unsigned();         
            $table->string('nombres',30);
            $table->string('apellidoP',30);
            $table->string('apellidoM',30);
            $table->integer('empresas_id')->unsigned();
            $table->string('curp');
            $table->string('edad');
            $table->string('generacion',15);
            $table->string('rfc');
            $table->string('nss');
            $table->string('ife');
            $table->string('telefono',10);
            $table->string('salario');
            $table->string('salarioPPP');           
            $table->string('contrato_individual');
            $table->string('acta_nac');
            $table->string('comprobante_dom');
            $table->string('solicitud_pensiones');

            $table->foreign('empresas_id')
              ->references('id')
              ->on('empresas'); 
            $table->foreign('empresaremun_id')
              ->references('id')
              ->on('empresaremuns');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleados');
    }

}
