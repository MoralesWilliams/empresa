<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresas', function(Blueprint $table)
		{

			$table->increments('id');
			$table->string('razon_social',50);
			$table->string('domicilio',30);
			$table->string('rfc_em_l',30);
			$table->string('representante_l',30);
			$table->string('num_contacto',50);
			$table->string('nombre_contacto',30);
			$table->string('num_registro_p',30);
			$table->string('acta_constitutiva',3);
			$table->string('poder_notarial',3);
			$table->string('ife_representante_l',3);
			$table->string('satic_psi',5);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresas');
	}

}
