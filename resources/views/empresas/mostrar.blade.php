@extends('app')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="form-group col-md-10">
      <h4>REGISTRO DE EMPRESAS LABORALES ACTUALES</h4>
    </div>
    <div class="form-group col-md-2">
      <p class="alert alert-success">Total:{{ $total}}</p>
    </div>  
  </div>
    <div class="col-md-12">
      <table id="table_show" class="display table table-striped" cellspacing="0" width="100%">
         <caption>EMPRESAS LABORALES</caption>
        <thead>
          <tr>
            <th>RAZON SOCIAL</th>
            <th>TOTAL DE EMPLEADOS</th>
            <th>ACCION</th>
          </tr>
          <thead>
            <tbody>
          @foreach($grupos as $grupo)
          <tr>
            <td>{{$grupo->razon_social}}</td>
            <td>{{$grupo->total}}</td>
            <td><button onclick="empleados({{$grupo->id}})" class="btn btn-sm btn-info" >Empleados</a></td>
          </tr>
          @endforeach
          <tbody>
      </table>
   <!--    <button onclick="$('.z').empty();" class="close"  data-dismiss="alert">X</button>
    --> </div>
   <div class="col-md-12 z"> 
  </div>
<!--  ////////////////////////////////////////////////////////// -->
<!-- Modal Dialog -->
<!-- Modal -->
 <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel"> Empleados </h4>
       </div>
       <div class="modal-body" id="getCode" style="overflow-x: scroll;">
       </div>
    </div>
   </div>
 </div>
<!--  ////////////////////////////////////////////////////////// -->
@endsection
@section('scripts')
  {!!  Html::script('js/bootstrapValidator.js') !!}
  {!!  Html::script('js/es_ES.js') !!}
  {!!  Html::script('js/selectize.js') !!}
<script  type="text/javascript">
function empleados(d) { 
  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
$.ajax({
                    url : "/empresas/buscar", 
                    type:"POST", 
                    data:{'d':d},                  
                    success : function(data){  
                          $(".z").empty();
                          $(".z").append("<div class='row'><div class='form-group col-md-10'><h4>Empleados de la empresa: "+data[0]['empresa'][0]['razon_social']+"</h4></div><div class='form-group col-md-2'><p class='alert alert-success'>Total:"+data[0]['total']+"</p></div></div><table id='results' class='display table table-striped' cellspacing='0' width='100%'></table>");
                          $("#results").append("<thead><tr><th>NOMBRE</th><th>CURP</th><th>RFC</th><th>NSS</th></tr></thead>"); for (var i = 0; i<data[0]['nombres'].length ; i ++) {
                          $("#results").append("</tbody><tr><td>" + data[0]['nombres'][i] + "</td><td>" + data[0]['curp'][i] + "</td><td>" + data[0]['rfc'][i] + "</td><td>" + data[0]['nss'][i] + "</td></tr></tbody></table>");
                            }
                            //modal empleados
                          $(".modal-body").html($(".z"));
                          $("#getCodeModal").modal('show');

                    },error:function(){ 
            alert("error!!!!");
        }  
      }).done(function(){
           $(document).ready(function() {
         $('#results').DataTable( {
                dom: 'Bfrtip',
                "order": [[ 0, "asc" ]],
                "language": {
                "url": "{{asset('/json/Spanish.json')}}"},
                "pagingType": "full_numbers",          
                buttons: [
                    {
                        extend: 'excelFlash',
                        title: 'Data export'
                    },
                    {
                        extend: 'pdfFlash',
                        title: 'Data export'
                    },
                    'colvis'
                ]
            } );
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $.fn.dataTable.tables( { api: true } )
        .buttons.resize();
})
    } );
      });
}
</script>
@endsection               
