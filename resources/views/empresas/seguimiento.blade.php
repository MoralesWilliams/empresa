@extends('app')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="form-group col-md-10">
      <h4>SEGUIMIENTO DE EMPRESAS CLIENTE </h4>
    </div>
    <div class="form-group col-md-2">
      <p class="alert alert-success">Total:{{ $total}}</p>
    </div>  
  </div>
    <div class="col-md-12 table-responsive">
     <table id="example" class="display table table-striped seguimiento" cellspacing="0" width="100%">
         <caption>EMPRESAS LABORALES</caption>
        <thead>
          <tr>
            <th>EMPRESA<br>LABORAL</th>
            <th>SALARIO</th>
            <th>COMISION<br>(8%)</th>
            <th>IVA<br>(16%)</th>
            <th>TOTAL1<br>(100%)</th>            
            <th>CONTINGENCIA<br>(20%)</th>            
            <th>GANANCIA<br>(80%)</th>                        
            <th>TOTAL2<br>(100%)</th>                        
            <th>VENDEDORES<br>(30%)</th>                        
            <th>CISS<br>(70%)</th>                                    
            <th>EMPLEADOS</th>                                    
          </tr>
          <thead>  
          <tfoot>
              <tr>
                  <th style="text-align:right">Totales:</th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>
                  <th style="text-align:right"></th>                  
                  <th style="text-align:right"></th>                                    
              </tr>
          </tfoot>         
            <tbody>
          @foreach($seguimiento as $segui)
          <tr>
            <td style="text-align:center" >{{$segui->razon_social}}</td>
            <td style="text-align:center">{{$segui->salarioppp}}</td>            
            <td style="text-align:center">{{$segui->comision}}</td>
            <td style="text-align:center">{{$segui->iva}}</td>
            <td style="text-align:center">{{$segui->total1}}</td>
            <td style="text-align:center">{{$segui->contingencia}}</td>
            <td style="text-align:center">{{$segui->ganancia}}</td>            
            <td style="text-align:center">{{$segui->ganancia}}</td>            
            <td style="text-align:center">{{$segui->vendedores}}</td>            
            <td style="text-align:center">{{$segui->ciss}}</td>     
            <td style="text-align:center"><button onclick="empleados({{$segui->id}})" class="btn btn-sm btn-info" ><span class="glyphicon glyphicon-user"></span></button></td>                   
          </tr>
          @endforeach         
          <tbody>
      </table>
    </div>
       <div class="col-md-12 z"> 
  </div>
<!--  ////////////////////////////////////////////////////////// -->
<!-- Modal Dialog -->
<!-- Modal -->
 <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel"> Empleados </h4>
       </div>
       <div class="modal-body" id="getCode" style="overflow-x: scroll;">
       </div>
    </div>
   </div>
 </div>
<!--  ////////////////////////////////////////////////////////// -->
@endsection
@section('scripts')
  {!!  Html::script('js/bootstrapValidator.js') !!}
  {!!  Html::script('js/es_ES.js') !!}
  {!!  Html::script('js/selectize.js') !!}
<script  type="text/javascript">
function empleados(d) {  
  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
$.ajax({
                    url : "/empresas/seguimiento", 
                    type:"POST", 
                    data:{'d':d},                  
                    success : function(data){ 
                          $(".z").empty();
                          $(".z").append("<div class='row'><div class='form-group col-md-10'><h4>Empleados de la empresa: "+data[0]['empresa'][0]['razon_social']+"</h4></div><div class='form-group col-md-2'><p class='alert alert-success'>Total:"+data[0]['total']+"</p></div></div><table id='results' class='display table table-striped' cellspacing='0' width='100%'><caption>Empleados </caption></table>");
                          $("#results").append("<thead><tr><th>nombres</th><th>salario</th><th>comision<br>(8%)</th><th>iva<br>(16%)</th><th>total1<br>(100%)</th><th>contingencia<br>(20%)</th><th>ganancia<br>(80%)</th><th>total2<br>(100%)</th><th>vendedores<br>(30%)</th><th>ciss<br>(70%)</th></tr></thead>"); for (var i = 0; i<data[0]['nombres'].length ; i ++) {
                          $("#results").append("</tbody><tr><td>" + data[0]['nombres'][i] + "</td><td>" + data[0]['salarioppp'][i] + "</td><td>" + data[0]['comision'][i] + "</td><td>" + data[0]['iva'][i]+ "</td><td>" + data[0]['total1'][i]+ "</td><td>" + data[0]['contingencia'][i]+ "</td><td>" + data[0]['ganancia'][i]+ "</td><td>" + data[0]['total2'][i]+ "</td><td>" + data[0]['vendedores'][i]+ "</td><td>" + data[0]['ciss'][i] + "</td></tr></tbody></table>");
                            }
                            //modal empleados
                          $(".modal-body").html($(".z"));
                          $("#getCodeModal").modal('show');

                    },error:function(){ 
            alert("error!!!!");
        }  
      }).done(function(){
           $(document).ready(function() {
         $('#results').DataTable( {
                dom: 'Bfrtip',
                "order": [[ 0, "asc" ]],
                    "language": {
                "url": "{{asset('/json/Spanish.json')}}"},
                "pagingType": "full_numbers",          
                buttons: [
                    {
                        extend: 'excelFlash',
                        title: 'Data export'
                    },
                    {
                        extend: 'pdfFlash',
                        title: 'Data export'
                    }
                ]
            } );
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $.fn.dataTable.tables( { api: true } )
        .buttons.resize();
})
    } );
      });
}
</script>
@endsection               
