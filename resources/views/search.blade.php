@extends('app')
@section('content')
<div class="container-fluid">
  <h4>Nuevo Empleado</h4>
  <div class="col-md-18">
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
        <li class="bg-danger">{{ $error }}</li>
      @endforeach
    @endif
  {{Form::open(['action'=>['UsersController@searchUser'],'method'=>'GET'])}}
  {{Form::text('q','',['id'=>'q','placeholder'=>'Enter name'])}}
  {{Form::close()}}
  </div>
</div>
@endsection					  		
<style>
input.uppercase{
text-transform: uppercase;
}
</style>                      
<script type="text/javascript">
$(function()
{
  $("#q").autocomplete({
    source:"{{URL::route('autocomplete')}}",
    minLength:3,
    select:function(event,ui)
    {
      $('#q').val(ui.item.value);
       $("#y").val(ui.item.value);
    }
  });
});
</script>