@extends('app')
@section('content')
<div class="container-fluid">
  <h4>Nuevo Empleado</h4>
  <div class="col-md-18">
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
        <li class="bg-danger">{{ $error }}</li>
      @endforeach
    @endif
    {!! Form::open(array('url' => 'empleados/nuevo','role' => 'form','id' => 'empleado_nuevo')) !!}
      <div class="row">
        <div class="form-group col-md-4">     
          {!! Form::label('empresaPPP2', 'Empresa Remuneradora',array('class' => 'class')) !!}
          {!! Form::text('empresaPPP2',old('empresaPPP2'), array('class' => 'form-control','id'=>'empresaPPP2')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('apellido_paterno', 'Apellido paterno',array('class' => 'class')) !!}
          {!! Form::text('apellido_paterno', old('apellido_paterno'), array('class' => 'form-control')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('apellido_materno', 'Apellido materno',array('class' => 'class')) !!}
          {!! Form::text('apellido_materno', old('apellido_materno'), array('class' => 'form-control')) !!}
        </div>  
      </div>
      <div class="row">
      	<div class="form-group col-md-4">
          {!! Form::label('nombre', 'Nombre',array('class' => 'class')) !!}
          {!! Form::text('nombre', old('nombre'), array('class' => 'form-control','id'=>'nombre')) !!}
        </div> 
         <div class="form-group col-md-4">
          {!! Form::label('empresa', 'Empresa laboral',array('class' => 'class')) !!}
          {!! Form::select('empresa',$empresas, old('empresa'), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-md-4">
          {!! Form::label('telefono', 'Telefono de casa o celular',array('class' => 'class')) !!}
          {!! Form::text('telefono', old('telefono'), array('class' => 'form-control')) !!}
        </div> 
      </div>  
      <div class="row">
      	<div class="form-group col-md-4">
          {!! Form::label('curp1', 'Curp',array('class' => 'class')) !!}
          {!! Form::text('curp1', old('curp1'), array('class' => 'form-control uppercase')) !!}
        </div>          
        <div class="form-group col-md-4">
          {!! Form::label('rfc1', 'RFC',array('class' => 'class')) !!}
          {!! Form::text('rfc1', old('rfc1'), array('class' => 'form-control uppercase')) !!}
        </div>
        <div class="form-group col-md-4">
          {!! Form::label('nss1', 'Numero de seguro social',array('class' => 'class')) !!}
          {!! Form::text('nss1', old('nss1'), array('class' => 'form-control uppercase')) !!}
        </div>  
      </div>   
      <div class="row">      	
         <div class="form-group col-md-4">
          {!! Form::label('salario', 'Salario Diario Integrado',array('class' => 'class')) !!}
          {!! Form::text('salario',old('salario'), array('class' => 'form-control','placeholder'=>'cantidad en pesos')) !!}
        </div> 
	<div class="form-group col-md-4">
		{!! Form::label('tipo_salario', 'Tipo de pago ',array('class' => 'class')) !!}
		{!! Form::select('tipo_salario',['1' => 'SEMANAL','2' => 'QUINCENAL','3' => 'MENSUAL'], null, ['class' => 'form-control']) !!}
	</div>       
         <div class="form-group col-md-4">
          {!! Form::label('salarioPPP', 'Beneficio PPP ',array('class' => 'class')) !!}
          {!! Form::text('salarioPPP',old('salarioPPP'), array('class' => 'form-control','placeholder'=>'cantidad en pesos')) !!}
        </div>                         
        </div>
        <div class="row">   
       	<div class="form-group col-md-4">
		{!! Form::label('tipo_trabajador', 'Tipo de Trabajador ',array('class' => 'class')) !!}
		{!! Form::select('tipo_trabajador',['1' => 'Obrero','2' => 'Supervisor','3' => 'Jefatura'], null, ['class' => 'form-control']) !!}
	 </div>  
       <div class="form-group col-md-4">
                {!! Form::label('tipo_sangre', 'Tipo de Sangre: ',array('class' => 'class')) !!}
		{!! Form::select('tipo_sangre',['1' => 'O+','2' => 'O-','3' => 'A+','4' => 'A-','5' => 'B+','6' => 'B-','7' => 'AB+','8' => 'AB-'], null, ['class' => 'form-control']) !!}
        </div>                         
       <div class="form-group col-md-4">
          {!! Form::label('donador_organos', '¿Es Usted Donador de Organos?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('donador_organos','si') !!} SI
          {!! Form::radio('donador_organos','no') !!} NO
        </div>                         
       </div>
	<div class="row">   
         <div class="form-group col-md-4">   
          {!! Form::label('contrato_individual', '¿Cuenta con Contrato individual de trabajo?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('contrato_individual','si') !!} SI
          {!! Form::radio('contrato_individual','no') !!} NO
         </div>          
         <div class="form-group col-md-4">   
          {!! Form::label('acta_nac', '¿Cuenta con Acta de nacimiento?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('acta_nac','si') !!} SI
          {!! Form::radio('acta_nac','no') !!} NO
         </div> 
	<div class="form-group col-md-4">   
          {!! Form::label('comprobante_dom', '¿Cuenta con Comprobante de domcilio?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('comprobante_dom','si') !!} SI
          {!! Form::radio('comprobante_dom','no') !!} NO
         </div>        
        </div>        
        <div class="row">	  
	 <div class="form-group col-md-4">   
          {!! Form::label('ife1', '¿Cuenta con: ?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('ife1','ine') !!} INE
          {!! Form::radio('ife1','ife') !!} IFE
         </div>
         <div class="form-group col-md-4">   
          {!! Form::label('solicitud_pensiones', '¿Cuenta con Solicitud de afiliacion al fondo de pensiones?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('solicitud_pensiones','si') !!} SI
          {!! Form::radio('solicitud_pensiones','no') !!} NO
         </div>
	<div class="form-group col-md-4">   
          {!! Form::label('fonacot', '¿Cuenta con: ?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('fonacot','fonacot') !!} FONACOT
          {!! Form::radio('fonacot','infonacot') !!} INFONACOT
         </div>                   
        </div>
       <div class="row">
        <div class="form-group col-md-4">
          {!! Form::label('archivo', 'EXPEDIENTE CLINICO',array('class' => 'class')) !!}
          <input type="file" name="archivo" id="archivo" value="{{old('archivo')}}" accept="application/pdf"/>
        </div>
         <div class="form-group col-md-4">
          {!! Form::label('archivo2', 'CARTA BAJO PROTESTA',array('class' => 'class')) !!}
          <input type="file" name="archivo2" id="archivo2" value="{{old('archivo2')}}" accept="application/pdf"/>
        </div>
	<div class="form-group col-md-4">
          {!! Form::label('archivo4', 'FORMATO PLAN PRIVADO DE PENSIONES',array('class' => 'class')) !!}
          <input type="file" name="archivo4" id="archivo4" value="{{old('archivo4')}}" accept="application/pdf"/>
        </div>
      </div>
 	<div class="row">	  
	<div class="form-group col-md-4">
          {!! Form::submit('Agregar empleado', array('class' => 'btn btn-primary pull-right')) !!}
      	 </div>
         <div class="form-group col-md-4" style="visibility:hidden">
          {!! Form::text('id_empresaPPP2',old('id_empresaPPP2'), array('class' => 'form-control','id'=>'id_empresaPPP2','readonly')) !!}
         </div>
	</div>
      {!! Form::close() !!}
  </div>
</div>
@endsection					  		
@section('scripts')
  {!!  Html::script('js/bootstrapValidator.js') !!}
  {!!  Html::script('js/es_ES.js') !!}
  {!!  Html::script('js/selectize.js') !!}

<script  type="text/javascript">
$( '#empresaPPP2' ).autocomplete({
  source:'http://159.203.93.164/search',
  minLength: 1,
  appendTo: "id",
  select:function(event,ui){
    $('#empresaPPP2').val(ui.item.value);
     $("#id_empresaPPP2").val(ui.item.id);
  }  
});
$(document).ready(function() {

        $('#empleado_nuevo').bootstrapValidator({
            fields: {
                empresaPPP2: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 50,
                        },
                        regexp: {
                regexp:/^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/,
                message: 'Solo se aceptan letras y numeros'
            },
                    }
                },
              apellido_paterno: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 20,
                        },
                        regexp: {
                regexp:/^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/,
                message: 'Solo letras'
                },
                    }
                },
                apellido_materno: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 20,
                        },
                        regexp: {
                regexp:/^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/,
                message: 'Solo letras'
            },
                    }
                },
                nombre: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 30,
                        },
                        regexp: {
                regexp:/^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/,
                message: 'Solo letras'
            },
                    }
                },                
                empresa: {
                    validators: {
                        notEmpty: {
                        },
                    }
                },
                 telefono: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 10,
                        },
                        regexp: {
                  regexp:/^[0-9]+$/,
                  message: 'Solo numeros'
                      },
                    }
                },
                curp1: {
                    validators: {
                        notEmpty:{                          
                        },
                        stringLength:{
                          max: 18,
                        },
                      regexp: {
                          regexp:/^[a-zA-Z]{4}((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229)(H|M|h|m)(AS|as|BC|bc|BS|bs|CC|cc|CL|cl|CM|cm|CS|cs|CH|ch|DF|df|DG|dg|GT|gt|GR|gr|HG|hg|JC|jc|MC|mc|MN|mn|MS|ms|NT|nt|NL|nl|OC|oc|PL|pl|QT|qt|QR|qr|SP|sp|SL|sl|SR|sr|TC|tc|TS|ts|TL|tl|VZ|vz|YN|yn|ZS|zs|SM|sm|NE|ne)([a-zA-Z]{3})([a-zA-Z0-9\s]{1})\d{1}$/,
                          message: 'Curp incorrecto'
                      },  
                            }
                },
                rfc1: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 13,
                        },
                        regexp: {
                              regexp:/^[a-zA-Z]{4}((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229)[a-zA-Z0-9]{3}$/,
                        message: 'Rfc incorrecto'
                        },
                    }
                },      
                nss1: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 11,
                        },                  
                        regexp: {
                          regexp:/^[0-9]+$/,
                          message: 'Solo numeros'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 10,
                        },                  
                        regexp: {
                          regexp:/^[0-9]+$/,
                          message: 'Solo numeros'
                        },                       
                    }
                },            
                salarioPPP: {
                    validators: {
                        notEmpty: {
                        },
                        stringLength:{
                          max: 10,
                        },                  
                       regexp: {
                          regexp:/^[0-9]+$/,
                          message: 'Solo numeros'                        
                        },
                  }
                },
                ife1: {
                    validators: {
                        notEmpty: {
                          message: 'Selecciona una opcion'
                        },
                      }
                },
                contrato_individual: {
                    validators: {
                        notEmpty: {
                          message: 'Selecciona una opcion'                          
                        },
                    }
                },
                acta_nac: {
                    validators: {
                        notEmpty: {
                          message: 'Selecciona una opcion'                          
                        },
                    }
                },
                comprobante_dom: {
                    validators: {                  
                        notEmpty: {
                          message: 'Selecciona una opcion'                          
                        },
                    }
                },
              solicitud_pensiones: {
                    validators: {                  
                        notEmpty: {
                          message: 'Selecciona una opcion'                          
                        },
                    }
                }                                          
              }
        });
    });
</script>
@endsection               
<style>
input.uppercase{
text-transform: uppercase;
}
</style>                      
