@extends('app')
@section('content')
<div class="container-fluid">
  <h4>SELECCIONE EMPRESA LABORAL</h4>
  <div class="col-md-18">
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
        <li class="bg-danger">{{ $error }}</li>
      @endforeach
    @endif
    {!! Form::open(array('url' => 'empleados/clasificar','role' => 'form','id' => 'empleados_clasificar')) !!}
      <div class="row">
      <div class="form-group col-md-4"> 
        {!! Form::label('buscar', 'Buscar',array('class' => 'class')) !!}
        {!! Form::select('buscar', $empresas,null, array('class' => 'form-control')) !!}       
      </div>
      </div>
      <div class="row">
      <div class="form-group col-md-2">
        {!! Form::submit('Buscar', array('class' => 'btn btn-primary pull-right')) !!}
        <!-- {!! Form::submit('Buscar', array('class' => 'btn btn-primary pull-right', 'onClick' => 'myFunction()')) !!} -->
      </div>      
      </div>
      {!! Form::close() !!}
  </div>
</div>
@endsection               

