@extends('app')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="form-group col-md-10">
      <h4>EMPLEADOS GENERACION AFORE</h4>
    </div>
    <div class="form-group col-md-2">
      <p class="alert alert-success">Total:{{ $total}}</p>
    </div>  
  </div>
    <div class="col-md-12">
     <table id="example" class="display table table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Nombres</th>
            <th>Curp</th>
            <th>Rfc</th>
            <th>Nss</th>
          </tr>
          <thead>
            <tbody>
          @foreach($empleados as $empleados)
          <tr>
            <td>{{$empleados->nombres."  ".$empleados->apellidoP."  ".$empleados->apellidoM}}</td>
            <td>{{$empleados->curp}}</td>
            <td>{{$empleados->rfc}}</td>
            <td>{{$empleados->nss}}</td>
          </tr>
          @endforeach
          <tbody>
      </table>
    </div>
  </div>
@endsection