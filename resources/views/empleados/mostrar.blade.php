@extends('app')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="form-group col-md-10">
      <h4>DATOS DE EMPLEADOS</h4>
    </div>
    <div class="form-group col-md-2">
      <p class="alert alert-success">Total:{{ $total}}</p>
    </div>  
  </div>
    <div class="col-md-12">
     <table id="table_show" class="display table table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>NOMBRE</th>
            <th>CURP</th>
            <th>RFC</th>
            <th>TELEFONO</th>
            <th>EDAD</th>
            <th>Accion</th>
          </tr>
          <thead>
            <tbody>
          @foreach($empleados as $empleado)
          <tr>
            <td>{{$empleado->nombres." ".$empleado->apellidoP." ".$empleado->apellidoM}}</td>
            <td>{{$empleado->curp}}</td>
            <td>{{$empleado->rfc}}</td>
            <td>{{$empleado->telefono}}</td>
            <td>{{$empleado->edad}}</td>
            <td><a href="{{url('/empleados/editar/'.$empleado->id)}}"class="btn btn-md btn-info ">Editar</a> </td>
          </tr>
          @endforeach
          <tbody>
      </table>
    </div>
@endsection
