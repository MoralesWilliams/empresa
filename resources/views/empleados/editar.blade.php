@extends('app')
@section('content')
<div class="container-fluid">
  <h4>Nuevo Empleado</h4>
  <div class="col-md-18">
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
        <li class="bg-danger">{{ $error }}</li>
      @endforeach
    @endif
    {!! Form::open(array('url' => 'empleados/editar','role' => 'form','id' => 'empleado_editar')) !!}
     <input type="hidden" name="id" id="id" value="{{$empleados->id}}" class="form-control"/> 
      <div class="row">
        <div class="form-group col-md-4">     
          {!! Form::label('empresaPPP2', 'Empresa Remuneradora',array('class' => 'class')) !!}
          {!! Form::select('empresaPPP2',$empresasremun,$empleados->empresaremun_id, array('class' => 'form-control','readonly')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('apellido_paterno', 'Apellido paterno',array('class' => 'class')) !!}
          {!! Form::text('apellido_paterno',  $empleados->apellidoP, array('class' => 'form-control')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('apellido_materno', 'Apellido materno',array('class' => 'class')) !!}
          {!! Form::text('apellido_materno',  $empleados->apellidoM, array('class' => 'form-control')) !!}
        </div>  
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          {!! Form::label('nombre', 'Nombre',array('class' => 'class')) !!}
          {!! Form::text('nombre',  $empleados->nombres, array('class' => 'form-control')) !!}
        </div> 
         <div class="form-group col-md-4">
          {!! Form::label('empresa', 'Empresa laboral',array('class' => 'class')) !!}
          {!! Form::select('empresa',$empresas,$empleados->empresas_id, array('class' => 'form-control','readonly')) !!}
        </div>
        <div class="form-group col-md-4">
          {!! Form::label('telefono', 'Telefono de casa o celular',array('class' => 'class')) !!}
          {!! Form::text('telefono',  $empleados->telefono, array('class' => 'form-control')) !!}
        </div> 
      </div>  
      <div class="row">
        <div class="form-group col-md-4">
          {!! Form::label('curp1', 'Curp',array('class' => 'class')) !!}
          {!! Form::text('curp1',  $empleados->curp, array('class' => 'form-control uppercase')) !!}
        </div>          
        <div class="form-group col-md-4">
          {!! Form::label('rfc1', 'RFC',array('class' => 'class')) !!}
          {!! Form::text('rfc1',  $empleados->rfc, array('class' => 'form-control uppercase')) !!}
        </div>
        <div class="form-group col-md-4">
          {!! Form::label('nss1', 'Numero de seguro social',array('class' => 'class')) !!}
          {!! Form::text('nss1',  $empleados->nss, array('class' => 'form-control uppercase')) !!}
        </div>  
      </div>   
      <div class="row">       
         <div class="form-group col-md-4">
          {!! Form::label('salario', 'Salario ',array('class' => 'class')) !!}
          {!! Form::text('salario',$empleados->salario, array('class' => 'form-control')) !!}
        </div>        
         <div class="form-group col-md-4">
          {!! Form::label('salarioPPP', 'Salario PPP',array('class' => 'class')) !!}
          {!! Form::text('salarioPPP',$empleados->salarioPPP, array('class' => 'form-control')) !!}
        </div>                
         <div class="form-group col-md-4">   
          {!! Form::label('ife1', '¿Cuenta con IFE?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('ife1','si',$empleados->ife) !!} SI
          {!! Form::radio('ife1','no',$empleados->ife) !!} NO
         </div>
        </div>
        <div class="row">       
         <div class="form-group col-md-4">   
          {!! Form::label('contrato_individual', '¿Cuenta con Contarto individual de trabajo?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('contrato_individual','si',$empleados->contrato_individual) !!} SI
          {!! Form::radio('contrato_individual','no',$empleados->contrato_individual) !!} NO
         </div>          
         <div class="form-group col-md-4">   
          {!! Form::label('acta_nac', '¿Cuenta con Acta de nacimiento?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('acta_nac','si',$empleados->acta_nac) !!} SI
          {!! Form::radio('acta_nac','no',$empleados->acta_nac) !!} NO
         </div>
         <div class="form-group col-md-4">   
          {!! Form::label('comprobante_dom', '¿Cuenta con Comprobante de domcilio?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('comprobante_dom','si',$empleados->comprobante_dom) !!} SI
          {!! Form::radio('comprobante_dom','no',$empleados->comprobante_dom) !!} NO
         </div>
        </div>        
        <div class="row">
         <div class="form-group col-md-4">   
          {!! Form::label('solicitud_pensiones', '¿Cuenta con Solicitud de afiliacion al fondo de pensiones?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('solicitud_pensiones','si',$empleados->solicitud_pensiones) !!} SI
          {!! Form::radio('solicitud_pensiones','no',$empleados->solicitud_pensiones) !!} NO
         </div>          
         <div class="form-group col-md-4">
          {!! Form::submit('Editar empleado', array('class' => 'btn btn-primary pull-right')) !!}
         </div>
        </div>
      {!! Form::close() !!}
  </div>
</div>
@endsection               
<style>
input.uppercase{
text-transform: uppercase;
}
</style>                      
