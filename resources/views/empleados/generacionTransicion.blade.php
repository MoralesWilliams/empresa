@extends('app')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="form-group col-md-10">
      <h4>EMPLEADOS GENERACION TRANSICION</h4>
    </div>
    <div class="form-group col-md-2">
      <p class="alert alert-success">Total:{{ $total}}</p>
    </div>  
  </div>
    <div class="col-md-12">
     <table id="table_show" class="display table table-striped y" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>NOMBRE</th>
            <th>CURP</th>
            <th>RFC</th>
            <th>NSS</th>
          </tr>
          <thead>
            <tbody>
          @foreach($empleados as $empleados)
          <tr>
            <td>{{$empleados->nombres."  ".$empleados->apellidoP."  ".$empleados->apellidoM}}</td>
            <td>{{$empleados->curp}}</td>
            <td>{{$empleados->rfc}}</td>
            <td>{{$empleados->nss}}</td>
          </tr>
          @endforeach
          <tbody>
      </table>
    </div>
  </div>
@endsection