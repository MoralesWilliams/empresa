@extends('app')
@section('content')
<div class="container-fluid">
  <h4>DATOS DE LA EMPRESA REMUNERADORA</h4>
  <div class="col-md-18">
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
        <li class="bg-danger">{{ $error }}</li>
      @endforeach
    @endif
    {!! Form::open(array('url' => 'empresaRemuneradora/nuevo','role' => 'form','id' => 'empresas_remuneradoras_nuevo')) !!}
      <div class="row">
        <div class="form-group col-md-4">
          {!! Form::label('razon_social', 'Razon social',array('class' => 'class')) !!}
          {!! Form::text('razon_social', old('razon_social'), array('class' => 'form-control')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('domicilio', 'Domicilio',array('class' => 'class')) !!}
          {!! Form::text('domicilio', old('domicilio'), array('class' => 'form-control')) !!}
        </div> 
        <div class="form-group col-md-4">
          {!! Form::label('rfc_em_l', 'RFC de la empresa',array('class' => 'class')) !!}
          {!! Form::text('rfc_em_l', old('rfc_em_l'), array('class' => 'form-control')) !!}
        </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          {!! Form::label('representante_l', 'Representante Legal',array('class' => 'class')) !!}
          {!! Form::text('representante_l', old('representante_l'), array('class' => 'form-control')) !!}
        </div>  
        <div class="form-group col-md-4">
          {!! Form::label('num_registro_p', 'Numero de registro patronal ',array('class' => 'class')) !!}
          {!! Form::text('num_registro_p', old('num_registro_p'), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-md-4">   
          {!! Form::label('acta_constitutiva', '¿Cuenta con acta constitutiva?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('acta_constitutiva','si') !!} SI
          {!! Form::radio('acta_constitutiva','no') !!} NO
        </div>           
       </div>
      <div class="row">              
      <div class="form-group col-md-4">   
          {!! Form::label('poder_notarial', '¿Cuenta con poder notarial?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('poder_notarial','si') !!} SI
          {!! Form::radio('poder_notarial','no') !!} NO
      </div>             
      <div class="form-group col-md-4">   
          {!! Form::label('ife_representante_l', '¿Cuenta con ife del representante legal?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('ife_representante_l','si') !!} SI
          {!! Form::radio('ife_representante_l','no') !!} NO
      </div> 
      <div class="form-group col-md-4">   
          {!! Form::label('satic_psi', '¿Cuenta con SATIC  ò PSI?',array('class' => 'class')) !!}     
          <br>
          {!! Form::radio('satic_psi','satic') !!} SATIC
          {!! Form::radio('satic_psi','psi') !!} PSI
      </div> 
      </div>
      <div class="row">
      <div class="form-group col-md-4">
          {!! Form::submit('Agregar empresa remuneradora', array('class' => 'btn btn-primary pull-right')) !!}
      </div>      
      </div>
      {!! Form::close() !!}
  </div>
</div>
@endsection               
                          
      			  		
					           			
			