<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('titulo')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta HTTP-EQUIV="Expires" content="0">
    <meta HTTP-EQUIV="Pragma" content="no-cache">
   	<meta name="_token" content="{{ csrf_token() }}"/>
    {{header("Access-Control-Allow-Headers: Content-Type")}}
    {{header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT')}}
    {{header('Cache-Control: no-store, no-cache, must-revalidate')}}
    {{header('Cache-Control: post-check=0, pre-check=0',false)}}
<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('titulo')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta HTTP-EQUIV="Expires" content="0">
    <meta HTTP-EQUIV="Pragma" content="no-cache">
   	<meta name="_token" content="{{ csrf_token() }}"/>
    {{header("Access-Control-Allow-Headers: Content-Type")}}
    {{header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT')}}
    {{header('Cache-Control: no-store, no-cache, must-revalidate')}}
    {{header('Cache-Control: post-check=0, pre-check=0',false)}}
    {{header('Pragma: no-cache')}}
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href='https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <!--
    <link href='//cdn.datatables.net/1.10.6/css/jquery.dataTables.css' rel='stylesheet' type='text/css'>
    <link href='//cdn.datatables.net/tabletools/2.2.4/css/dataTables.tableTools.css' rel='stylesheet' type='text/css'>
  -->
  </head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<label><img src="{{asset('imgs/logo_menu.png')}}" class="img-responsive"></label>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">INICIO</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
            @if(Auth::User()->tipo_usuario == 3)
						<li><a href="{{ url('/auth/register') }}">Registrar nuevo</a></li>
            @endif
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Salir</a></li>
							</ul>
						</li>				
				</ul>
			</div>
		</div>
	</nav>

	 <!--sidebar-->
	 <div class="row">
	 	<div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
          <div data-spy="affix" data-offset-top="45" data-offset-bottom="90">
            <ul class="nav" id="sidebar-nav">
           		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">AGREGAR NUEVO<b class="caret"></b></a>
          			<ul class="dropdown-menu">
          				<li><a href="{{ url('/empleados/nuevo') }}">Empleado</a></li>
              			<li><a href="{{ url('/empresas/nuevo') }}">Empresa Cliente</a></li>             
              			<li><a href="{{ url('/empresaRemuneradora/nuevo') }}">Empresa Remuneradora</a></li> 
        	  		</ul>
          		</li>	                       	            
           		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">MOSTRAR<b class="caret"></b></a>
          			<ul class="dropdown-menu">
		              	<li><a href="{{ url('/empleados/mostrar') }}">Empleados</a></li>             
              			<li><a href="{{ url('/empresas/mostrar') }}">Empresas Cliente</a></li>             
              			<li><a href="{{ url('/empresaRemuneradora/mostrar') }}">Empresas Remuneradoras</a></li>             
        	  		</ul>
          		</li>	                       	                      		
       <!--     		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">CLASIFICACION POR EMPRESA<b class="caret"></b></a>
          			<ul class="dropdown-menu">
		              	<li><a href="{{ url('/empleados/clasificar') }}">Cliente</a></li>             
        		      	<li><a href="{{ url('/empresaRemuneradora/clasificar') }}">Remuneradora</a></li>             
        	  		</ul>
          		</li> -->	                       	                      		          		
<!--            		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">CLASIFICACION POR GENERACIONES<b class="caret"></b></a>
          			<ul class="dropdown-menu">
              			<li><a href="{{ url('/empleados/afore') }}">Afore</a></li>             
              			<li><a href="{{ url('/empleados/transicion') }}">Transicion</a></li>             
        	  		</ul>
          		</li>	                       	                      		          		          		
 -->          <li id="seguimiento"><a href="{{ url('/empresaRemuneradora/generacion') }}">GENERACIONES</a></li>
              <li id="seguimiento"><a href="{{ url('/empresas/seguimiento') }}">SEGUIMIENTO</a></li>
            </ul>
           </div>
        </div><!--/sidebar-->
        <div class="col-sm-10">
        	@yield('content')
        </div>
      </div>        
	<!-- Scripts -->
		   <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
      <script src="//cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js"></script>
      <script src="https://cdn.datatables.net/fixedcolumns/3.2.0/js/dataTables.fixedColumns.min.js"></script>
      <script src="//cdn.datatables.net/buttons/1.1.0/js/buttons.colVis.min.js"></script>
      <script src="//cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
      <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
      <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
      <script src="//cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>      
  <!--  
      <script src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script>
      <script src="//cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.js" type="text/javascript" language="javascript"></script>
      -->
      <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
      <script src="{{ asset('js/highcharts.js') }}"></script>

	  <script type="text/javascript" language="javascript" class="init" >
		  $(document).ready(function() {
      $('.seguimiento').DataTable( {        
        dom: 'Bfrtip ',        
        "order": [[ 0, "asc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/Spanish.json"
            },
            "pagingType": "full_numbers",
        buttons: [
           'copyHtml5',
            {
                extend: 'excelFlash',
                title: 'altavista',
                footer: true
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                footer: true,
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },{
            extend:'print',
            text:'Imprimir',
            footer:true
            },
            {
            extend:'colvis',
            text:'Ocultar/Ver Columnas'
          }
        ],
        sScrollX: "100%",
         "footerCallback": function ( row, data, start, end ) {
            var api = this.api(), data;
            var c,d,e,f,g,h,i,j,k,z;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over all pages
            total = api.column( 1 ).data().reduce( function (a, b) {
            z= intVal(a) + intVal(b)
            return z.toFixed(3);
                }, 0 );
            // Total over this page
            pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
            c= intVal(a) + intVal(b) 
            return c.toFixed(3);}, 0 );
            pageTotal1 = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
            d= intVal(a) + intVal(b) 
            return d.toFixed(3);}, 0 );
            pageTotal2 = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
            e= intVal(a) + intVal(b)
            return e.toFixed(3);}, 0 );
            pageTotal3 = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
            f= intVal(a) + intVal(b)
            return f.toFixed(3);}, 0 );
            pageTotal4 = api.column( 5, { page: 'current'} ).data().reduce( function (a, b) {
            g= intVal(a) + intVal(b)
            return g.toFixed(3);}, 0 );
            pageTotal5 = api.column( 6, { page: 'current'} ).data().reduce( function (a, b) {
            h= intVal(a) + intVal(b)
            return h.toFixed(3);}, 0 );
            pageTotal6 = api.column( 7, { page: 'current'} ).data().reduce( function (a, b) {
            i= intVal(a) + intVal(b)
            return i.toFixed(3);}, 0 );
            pageTotal7 = api.column( 8, { page: 'current'} ).data().reduce( function (a, b) {
            j= intVal(a) + intVal(b)
            return j.toFixed(3);}, 0 );
            pageTotal8 = api.column( 9, { page: 'current'} ).data().reduce( function (a, b) {
            k= intVal(a) + intVal(b)
            return k.toFixed(3);}, 0 );
            // Update footer
            $( api.column( 1 ).footer() ).html('$'+pageTotal);
            $( api.column( 2 ).footer() ).html('$'+pageTotal1);            
            $( api.column( 3 ).footer() ).html('$'+pageTotal2);            
            $( api.column( 4 ).footer() ).html('$'+pageTotal3);
            $( api.column( 5 ).footer() ).html('$'+pageTotal4);
            $( api.column( 6 ).footer() ).html('$'+pageTotal5);
            $( api.column( 7 ).footer() ).html('$'+pageTotal6);
            $( api.column( 8 ).footer() ).html('$'+pageTotal7);
            $( api.column( 9 ).footer() ).html('$'+pageTotal8);            
            $( api.column( 10 ).footer() ).html('Total Salarios:'+'<br>'+'$'+ total
            );
        }
    } );
  $('#table_show').DataTable( {        
        dom: 'Bfrtip ',        
        "order": [[ 0, "asc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/Spanish.json"
            },
            "pagingType": "full_numbers",
        buttons: [
           'copyHtml5',
            {
                extend: 'excelFlash',
                title: 'altavista'              
            },{
            extend:'print',
            text:'Imprimir' 
            },
            {
            extend: 'colvis',
            text:'Ocultar/Ver Columnas'
            }
        ],
        sScrollX: "100%",         
    } );
} );
		  </script>

	@yield('scripts')

</body>
</html>