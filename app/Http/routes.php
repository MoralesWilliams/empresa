<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');
Route::get('search', 'EmpresaRemuneradoraController@search');
HTML::macro('menu_active', function($route, $matchExact = false)
{
if($matchExact && Request::is($route))
{
$active = "active";
}
elseif(!$matchExact && (Request::is($route . '/*') || Request::is($route)))
{
$active = "active";
}

else
{
$active = '';
}

return $active;
});

/*Route::resource('excel','ExcelController');
*/

Route::controllers([
	'auth'						 => 'Auth\AuthController',
	'password' 					 => 'Auth\PasswordController',
	'empleados'					 => 'EmpleadosController',
	'empresas' 					 => 'EmpresasController',
	'empresaRemuneradora'		 => 'EmpresaRemuneradoraController',
	'excel' 					 => 'ExcelController',
	
]);
