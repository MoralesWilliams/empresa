<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\empleados;
use App\empresas;
use App\empresaremun;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	}
	public function getEmpleadosexcel()
	{
		Excel::create('Empleados_altavista', function($excel) {
            $excel->sheet('Empleados', function($sheet) {
                $products = empleados::all(); 
                $sheet->fromArray($products); 
            });
        })->export('xls')->handleExport();;
	}
	public function getEmpresasexcel()
	{
		Excel::create('Empresas', function($excel) {
            $excel->sheet('Empresas', function($sheet) {
                $products = empresas::all(); 
                $sheet->fromArray($products); 
            });
        })->export('xls')->handleExport();;
	}
	public function getEmpresasremunexcel()
	{
		Excel::create('Empresas remuneradoras', function($excel) {
            $excel->sheet('Empresas_remuneradoras', function($sheet) {
                $products = empresaremun::all(); 
                $sheet->fromArray($products); 
            });
        })->export('xls')->handleExport();;
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
