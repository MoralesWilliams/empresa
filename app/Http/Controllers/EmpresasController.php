<?php namespace App\Http\Controllers;
use Validator;
use Request;
use App\empresas;
use App\empleados;
use Input;
use DB;
use App\empresaremun;
use Response;
class EmpresasController extends Controller {
	public function __construct()
	{
		$this->middleware('auth'); //tiene que estar logueado para entrar al controller
	}
	public function getIndex()
	{
		return view('eventos/index');
	}
	public function getNuevo()
	{
		return view('empresas/nuevo');
	}
	public function postNuevo()
	{
		$v = Validator::make(Request::all(), [
        'razon_social' 				=> 'required|regex:/^[A-Za-z\s\0-9]+$/|min:3|max:50',
        'domicilio' 		    	        => 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:100',
        'rfc_em_l'   				=> 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:30',
        'representante_l'   		        => 'required|regex:/^[A-Za-z\s]+$/|min:5|max:30',
        'num_contacto'   	                => 'required|regex:/^[0-9\s]+$/|min:5|max:10',
        'nombre_contacto'                       => 'required|regex:/^[A-Za-z\s]+$/|min:3|max:50',
        'num_registro_p'                        => 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:30',
        'acta_constitutiva'                     => 'required',
        'poder_notarial'                        => 'required',
        'ife_representante_l'                   => 'required',
        'satic'                                 => 'required',
	'psi'                                   => 'required',
        ]);
        if ($v->fails())
        {
            return redirect()->back()->withInput(Request::all())->withErrors($v->errors());
        }
        if($empresas = Empresas::create([
            'razon_social'              	  =>  Request::input('razon_social'),
            'domicilio'                		  =>  Request::input('domicilio'),
            'rfc_em_l'         		          =>  Request::input('rfc_em_l'),
            'representante_l'         		  =>  Request::input('representante_l'),
            'num_contacto'         	          =>  Request::input('num_contacto'),
            'nombre_contacto'                     =>  Request::input('nombre_contacto'),
            'num_registro_p'                      =>  Request::input('num_registro_p'),
            'acta_constitutiva'                   =>  Request::input('acta_constitutiva'),
            'poder_notarial'                      =>  Request::input('poder_notarial'),
            'ife_representante_l'                 =>  Request::input('ife_representante_l'),
            'satic'                               =>  Request::input('satic'),
	    'psi'                                 =>  Request::input('psi'),
            ]))
        return redirect('empresas/mostrar');
	}
	public function getMostrar()
    {
        $empresas = empresas::all();
        $grupos = DB::table('empleados')
                     ->join('empresas', 'empresas.id', '=', 'empleados.empresas_id')
                     ->select(DB::raw('count(*) as total,razon_social,empresas.id'))
                     ->groupBy('empresas_id')
                     ->get();
        $total=count($grupos); 
        return view('empresas.mostrar',['empresas'=>$empresas,'grupos'=>$grupos,'total'=>$total]);
    }
   public function postBuscar()
  {
            $d = Input::get('d');
            $data =array();
            $data1 =array();
            $data2 = array();
             $empresa = empresas::select('razon_social')
                                             ->where('id',$d)
                                             ->get();
            foreach (empleados::where('empresas_id',$d)->get() as $empleado) {
            $data1[]=$empleado->nombres." ".$empleado->apellidoP." ".$empleado->apellidoM;
            $data2[]=$empleado->curp;
            $data3[]=$empleado->rfc;
            $data4[]=$empleado->nss;
        }
            $total=count($data1);
            $data[]=array('nombres'=>$data1,'curp'=>$data2,'rfc'=>$data3,'nss'=>$data4,'total'=>$total,'empresa'=>$empresa);
           return response()->json($data);
       }   
    public function getSeguimiento()
    {
/*         $seguimiento = DB::table('empleados')
                     ->select(DB::raw('((salarioPPP)*8)/100 as comision,((salarioPPP)*16)/100 as iva,nombres,id'))
                     ->get();
*/          $seguimiento = DB::select( DB::raw("select id,razon_social,salarioppp,comision,iva,total1,contingencia,ganancia,
                                                ((ganancia)*30)/100 as vendedores,((ganancia)*70)/100 as ciss
                                                from(select id,razon_social,salarioppp,comision,iva,total1,((total1)*20)/100 as contingencia,
                                                ((total1)*80)/100 as ganancia
                                                from(select id,razon_social,salarioppp,comision,iva,(comision + iva) as total1
                                                from(select empresas.id as id,razon_social,sum(salarioppp) as salarioppp,(sum(salarioPPP)*8)/100 as comision,
                                                (sum(salarioPPP)*16)/100 as iva from empleados inner join empresas 
                                                on empresas.id=empleados.empresas_id group by empresas_id)as sub1)as sub2)as sub3"));
            $total = count($seguimiento);
            return view('empresas.seguimiento',['seguimiento'=>$seguimiento,'total'=>$total]);
    }
    public function postSeguimiento()
    {
                        $d = Input::get('d');
                        $data =array();
                        $data1 =array();
                        $data2 = array();
                        $empresa = empresas::select('razon_social')
                                             ->where('id',$d)
                                             ->get();
                        $seguimiento = DB::select( DB::raw("select nombres,salarioppp,comision,iva,total1,contingencia,ganancia,total2,
                                                ((total2)*30)/100 as vendedores,((total2)*70)/100 as ciss
                                                from(select nombres,salarioppp,comision,iva,total1,contingencia,ganancia,
                                                ganancia as total2
                                                from(select nombres,salarioppp,comision,iva,total1,((total1)*20)/100 as contingencia,
                                                ((total1)*80)/100 as ganancia
                                                from(select nombres,salarioppp,comision,iva,(comision + iva) as total1
                                                from(select nombres,salarioppp,((salarioPPP)*8)/100 as comision,
                                                ((salarioPPP)*16)/100 as iva  from empleados where empresas_id = ".$d.")as sub1)as sub2)as sub3)as sub4"));
                        foreach ($seguimiento as $empleado) {
                        $data1[]=$empleado->nombres;
                        $data2[]=$empleado->salarioppp;
                        $data3[]=$empleado->comision;
                        $data4[]=$empleado->iva;
                        $data5[]=$empleado->total1;
                        $data6[]=$empleado->contingencia;
                        $data7[]=$empleado->ganancia;
                        $data8[]=$empleado->total2;
                        $data9[]=$empleado->vendedores;
                        $data10[]=$empleado->ciss;                       
                        }
                        $total=count($seguimiento);
                        $data[]=array('nombres'=>$data1,'salarioppp'=>$data2,'comision'=>$data3,'iva'=>$data4,
                            'total1'=>$data5,'contingencia'=>$data6,'ganancia'=>$data7,'total2'=>$data8,'vendedores'=>$data9,'ciss'=>$data10,'empresa'=>$empresa,
                            'total'=>$total);
                       return response()->json($data);                   
    }   
    
}










