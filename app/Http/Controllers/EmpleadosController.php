<?php namespace App\Http\Controllers;
use Validator;
use Request;
use App\empleados;
use App\empresas;
use App\empresaremun;
use DB;
use Response;
class EmpleadosController extends Controller {


	public function __construct()
	{
		$this->middleware('auth'); //tiene que estar logueado para entrar al controller
	}
    public function getIndex()
    {
                $empleados = Empleados::all();
        return view('Empleados.buscar',['empleados'=>$empleados]);
        //return 'Este es el index de Eventos'; //aqui va la vista
    }

public function getClasificar()
	{
        $empresas = array('');
        foreach (empresas::all() as $empresa) {
            $empresas[$empresa->id] = $empresa->razon_social;
        }
        return view('Empleados.buscar',['empresas'=>$empresas]);
		//return 'Este es el index de Eventos'; //aqui va la vista
	}
    public function postClasificar()
    {
        $nombre =Request::input('buscar');
        $empleados = Empleados::where('empresas_id','=',$nombre)->paginate(10);
        $total=count($empleados);
        return view('Empleados.clasificacion',['empleados'=>$empleados,'total'=>$total]);
    }
    public function getNuevo()
	{
        $empresas = array('');
        foreach (empresas::all() as $empresa) {
                $empresas[$empresa->id] = $empresa->razon_social;
        }
        $empresasremun = array('');
        foreach (empresaremun::all() as $empresa) {
                $empresasremun[$empresa->id] = $empresa->razon_social;
        }
	return view('empleados/nuevo',['empresas' => $empresas,'empresasremun' => $empresasremun]);
	}
	public function postNuevo()
	{
		$v = Validator::make(Request::all(), [

        'empresaPPP2' 			   => 'required',
        'apellido_paterno' 		   => 'required|regex:/^[A-Za-z\s]+$/|min:4|max:30',
        'apellido_materno' 		   => 'required|regex:/^[A-Za-z\s]+$/|min:4|max:30',
        'nombre' 	               => 'required|regex:/^[A-Za-z\s]+$/|min:3|max:30',
        'empresa' 	               => 'required',
        'telefono'                 => 'required',
        'curp1'                    => array('required','regex:/\A[A-Z][AEIOUX][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][MH][A-Z][BCDFGHJKLMNÑPQRSTVWXYZ]{4}[0-9A-Z][0-9]\z/i'),			
        'ife1'                     => 'required',
        'rfc1'                     => 'required|regex:/^[A-Z0-9]+$/|max:13',
        'nss1'                     => 'required|regex:/^[0-9]+$/|max:10',
        'salario'                  => 'required|regex:/^[0-9]+$/|max:100',
	'tipo_salario'             => 'required',
        'salarioPPP'               => 'required|regex:/^[0-9]+$/|max:100',
	'tipo_trabajador'          => 'required',
	'tipo_sangre'              => 'required',
	'donador_organos'          => 'required',
        'contrato_individual'      => 'required',
        'acta_nac'                 => 'required',
        'comprobante_dom'          => 'required',
        'solicitud_pensiones'      => 'required',
        'id_empresaPPP2'           => 'required',
	'fonacot'		   => 'required',
	'archivo'                  => 'mimes:PDF,pdf|max:50000',
        'archivo2'                 => 'mimes:PDF,pdf|max:50000',
       	'archivo4'                 => 'mimes:PDF,pdf|max:50000',

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput(Request::all())->withErrors($v->errors());
        }
	//archivos
	if(Request::file('archivo') != ""){
          $archivoPdf = str_random(30);
          $pdf = Request::file('archivo')->move('archivos/',$archivoPdf.'.pdf');
         }
   	 if(Request::file('archivo2') != ""){
          $archivoPdf2 = str_random(30);
          $pdf2 = Request::file('archivo2')->move('archivos/',$archivoPdf2.'.pdf');
         }
	 if(Request::file('archivo4') != ""){
          $archivoPdf4 = str_random(30);
          $pdf4 = Request::file('archivo4')->move('archivos/',$archivoPdf4.'.pdf');
          }
        //fecha de nacimiento
        $curp = Request::input('curp1');
        $anyoCurp=substr($curp,4,2);
        $mesCurp=substr($curp,6,2);
        $diaCurp=substr($curp,8,2);
        $anyoCurp=(int)$anyoCurp;
        $anyoCurp= $anyoCurp + 1900;
        if($anyoCurp < 1909)
        {
        $anyoCurp = $anyoCurp + 100;
        }
        $fechaNac=$anyoCurp."/".$mesCurp."/".$diaCurp;
         //  sacar edad de fecha
        list($ano,$mes,$dia) = explode("/",$fechaNac);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($dia_diferencia < 0 || $mes_diferencia < 0)
        $ano_diferencia--;
        $x =$ano_diferencia;
        //afore o transicion
        $nss = Request::input('nss1');
        $aforetrans=substr($nss,2,2);
        if($aforetrans > 25 && $aforetrans < 97)
        $generacion='transicion';
        else
        $generacion='afore';
        if($empresas = Empleados::create([
            'empresaremun_id'           =>  Request::input('id_empresaPPP2'),
            'nombres'                    =>  Request::input('nombre'),
            'apellidoP'                  =>  Request::input('apellido_paterno'),
            'apellidoM'                  =>  Request::input('apellido_materno'),
            'empresas_id'                =>  Request::input('empresa'),
            'curp'                       =>  Request::input('curp1'),
            'edad'                       =>  $x,
            'generacion'                 =>  $generacion,
            'rfc'                        =>  Request::input('rfc1'),
            'nss'                        =>  Request::input('nss1'),
            'ife'                        =>  Request::input('ife1'),
            'telefono'                   =>  Request::input('telefono'),
            'salario'                    =>  Request::input('salario'), 
	    'tipo_salario'               =>  Request::input('tipo_salario'),                       
            'salarioPPP'                 =>  Request::input('salarioPPP'),   
	    'tipo_trabajador'            =>  Request::input('tipo_trabajador'),    
	    'tipo_sangre'                =>  Request::input('tipo_sangre'),
	    'donador_organos'            =>  Request::input('donador_organos'),                               
            'contrato_individual'        =>  Request::input('contrato_individual'),                       
            'acta_nac'                   =>  Request::input('acta_nac'),                       
            'comprobante_dom'            =>  Request::input('comprobante_dom'),                       
            'solicitud_pensiones'        =>  Request::input('solicitud_pensiones'), 
	    'fonacot'                    =>  Request::input('fonacot'),                      
            ]))
        return redirect('empleados/mostrar');
	}
     public function getEditar($id = null){
        $empleados = empleados::find($id);
        $empresasremun = array();
        foreach (empresaremun::all() as $empresaremun) {
            $empresasremun[$empresaremun->id] = $empresaremun->razon_social;
        }
        $empresas = array();
        foreach (empresas::all() as $empresa) {
            $empresas[$empresa->id] = $empresa->razon_social;
        }
        return view('empleados.editar',['empleados'=>$empleados,'empresasremun'=>$empresasremun,'empresas'=>$empresas]);
     }
     public function postEditar()
    {
           $x= date('Y-m-d');
        $v = Validator::make(Request::all(), [
                'id'                       => 'required|exists:empleados',
                'empresaPPP2'              => 'required|exists:empresaremuns,id',
                'apellido_paterno'         => 'required|regex:/^[A-Za-z\s]+$/|min:5|max:30',
                'apellido_materno'         => 'required|regex:/^[A-Za-z\s]+$/|min:5|max:30',
                'nombre'                   => 'required|regex:/^[A-Za-z\s]+$/|min:3|max:30',
                'empresa'                  => 'required|exists:empresas,id',
                'telefono'                 => 'required',
                'curp1'                    => array('required','regex:/\A[A-Z][AEIOUX][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][MH][A-Z][BCDFGHJKLMNÑPQRSTVWXYZ]{4}[0-9A-Z][0-9]\z/i'),          
                'ife1'                     => 'required',
                'rfc1'                     => array('required','regex:/^[A-Z]{3,4}(\d{6})((\D|\d){3})?$/'),
                'nss1'                     => 'required|regex:/^[0-9]+$/|max:10',
                'salario'                  => 'required|regex:/^[0-9]+$/|max:100',
                'salarioPPP'               => 'required|regex:/^[0-9]+$/|max:100',                
                'contrato_individual'      => 'required',
                'acta_nac'                 => 'required',
                'comprobante_dom'          => 'required',
                'solicitud_pensiones'      => 'required',

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput(Request::all())->withErrors($v->errors());
        }
        //fecha de nacimiento
        $curp = Request::input('curp1');
        $anyoCurp=substr($curp,4,2);
        $mesCurp=substr($curp,6,2);
        $diaCurp=substr($curp,8,2);
        $anyoCurp=(int)$anyoCurp;
        $anyoCurp= $anyoCurp + 1900;
        if($anyoCurp < 1909)
        {
        $anyoCurp = $anyoCurp + 100;
        }
        $fechaNac=$anyoCurp."/".$mesCurp."/".$diaCurp;
         //  sacar edad de fecha
        list($ano,$mes,$dia) = explode("/",$fechaNac);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($dia_diferencia < 0 || $mes_diferencia < 0)
        $ano_diferencia--;
        $x =$ano_diferencia;
        //afore o transicion
        $nss = Request::input('nss1');
        $aforetrans=substr($nss,2,2);
        if($aforetrans > 25 && $aforetrans < 97)
        $generacion='transicion';
        else
        $generacion='afore';
        if(empleados::find(Request::input('id'))->update([
            'empresaremun_id'           =>  Request::input('empresaPPP2'),
            'nombres'                    =>  Request::input('nombre'),
            'apellidoP'                  =>  Request::input('apellido_paterno'),
            'apellidoM'                  =>  Request::input('apellido_materno'),
            'empresas_id'                =>  Request::input('empresa'),
            'curp'                       =>  Request::input('curp1'),
            'edad'                       =>  $x,
            'generacion'                 =>  $generacion,
            'rfc'                        =>  Request::input('rfc1'),
            'nss'                        =>  Request::input('nss1'),
            'ife'                        =>  Request::input('ife1'),
            'telefono'                   =>  Request::input('telefono'),
            'salario'                    =>  Request::input('salario'),                                              
            'salarioPPP'                 =>  Request::input('salarioPPP'),                                              
            'contrato_individual'        =>  Request::input('contrato_individual'),                                              
            'acta_nac'                   =>  Request::input('acta_nac'),                                              
            'comprobante_dom'            =>  Request::input('comprobante_dom'),                                              
            'solicitud_pensiones'        =>  Request::input('solicitud_pensiones'),                                              
            ]))                             
        return redirect('empleados/mostrar');
    }
    
     public function getAfore()
     {
        $empleados = Empleados::where('generacion','=','afore')->paginate(10);
         $total= count($empleados);
        return view('empleados.generacionAfore',['empleados'=>$empleados,'total' => $total]);
     }
     public function getTransicion()
     {
        $empleados = Empleados::where('generacion','=','transicion')->paginate(10);
        $total= count($empleados);
        return view('empleados.generacionTransicion',['empleados'=>$empleados,'total' => $total]);
     }
	 public function getMostrar()
    {
        $empleados = DB::table('empleados')
                     ->select(DB::raw('nombres,apellidoP,apellidoM,curp,telefono,rfc,edad,id'))
                     ->get();
        $total= count($empleados);
        return view('empleados.mostrar',['empleados'=>$empleados,'total'=>$total]);
    }
}









