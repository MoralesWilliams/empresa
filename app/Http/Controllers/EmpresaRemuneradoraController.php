<?php namespace App\Http\Controllers;
use Validator;
use Request;
use App\empleados;
use App\empresas;
use App\empresaremun;
use DB;
use Input;
use Response;

class EmpresaRemuneradoraController extends Controller {


	public function __construct()
	{
		$this->middleware('auth'); //tiene que estar logueado para entrar al controller
	}
	public function getIndex()
	{
		return view('eventos/index');
		//return 'Este es el index de Eventos'; //aqui va la vista
	}
    public function search()    
    {
       $term = Input::get('term');
        $results = array();
        $queries=DB::table('empresaremuns')
        ->where('razon_social','LIKE','%'.$term.'%')
        ->take(5)->get();
        foreach ($queries as $query) {
            $results[]=['id'=>$query->id,'label'=>$query->razon_social,'value'=>$query->razon_social];
        }
        return Response::json($results);
    }
	public function getNuevo()
	{
		return view('empresasRemuneradoras/nuevo');
	}
	public function postNuevo()
	{
		$v = Validator::make(Request::all(), [
        'razon_social' 				=> 'required|regex:/^[A-Za-z\s\0-9]+$/|min:3|max:50',
        'domicilio' 		    	=> 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:100',
        'rfc_em_l'   				=> 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:30',
        'representante_l'   		=> 'required|regex:/^[A-Za-z\s]+$/|min:5|max:30',
        'num_registro_p'            => 'required|regex:/^[A-Za-z\s\0-9]+$/|min:5|max:30',
        'acta_constitutiva'         => 'required',
        'poder_notarial'            => 'required',
        'ife_representante_l'       => 'required',
        'satic_psi'                 => 'required',
        ]);
        if ($v->fails())
        {
            return redirect()->back()->withInput(Request::all())->withErrors($v->errors());
        }
        if($empresas = empresaremun::create([
            'razon_social'              	  =>  Request::input('razon_social'),
            'domicilio'                		  =>  Request::input('domicilio'),
            'rfc_em_l'         		 		  =>  Request::input('rfc_em_l'),
            'representante_l'         		  =>  Request::input('representante_l'),
            'num_registro_p'                  =>  Request::input('num_registro_p'),
            'acta_constitutiva'               =>  Request::input('acta_constitutiva'),
            'poder_notarial'                  =>  Request::input('poder_notarial'),
            'ife_representante_l'             =>  Request::input('ife_representante_l'),
            'satic_psi'                       =>  Request::input('satic_psi'),
            ]))
        return redirect('empresaRemuneradora/mostrar');
	}
    public function getClasificar()
    {
        $empresaremun = array('');
        foreach (empresaremun::all() as $empresa) {
            $empresaremun[$empresa->id] = $empresa->razon_social;
        }
        return view('empresasRemuneradoras.buscar',['empresaremun'=>$empresaremun]);
        //return 'Este es el index de Eventos'; //aqui va la vista
    }
    public function postClasificar()
    {
        $nombre =Request::input('buscar');
        $empleados = Empleados::where('empresaremun_id','=',$nombre)->paginate(10);
        $total = count($empleados);
        return view('empresasRemuneradoras.clasificacion',['empleados'=>$empleados,'total'=>$total]);
    }
	public function getMostrar()
    {
        $empresas = empresaremun::all();
        $grupos = DB::table('empleados')
                     ->join('empresaremuns', 'empresaremuns.id', '=', 'empleados.empresaremun_id')
                     ->select(DB::raw('count(*) as total,razon_social,empresaremuns.id'))
                     ->groupBy('empresaremuns.id')
                     ->get();
        $total=count($grupos);
        return view('empresasRemuneradoras.mostrar',['empresas'=>$empresas,'grupos'=>$grupos,'total'=>$total]);
    }
     public function postBuscar()
    {
            $d = Input::get('d');
            $data =array();
            $data1 =array();
            $data2 = array();
             $empresa = empresaremun::select('razon_social')
                                             ->where('id',$d)
                                             ->get();
            foreach (empleados::where('empresaremun_id',$d)->get() as $empleado) {
            $data1[]=$empleado->nombres." ".$empleado->apellidoP." ".$empleado->apellidoM;
            $data2[]=$empleado->curp;
            $data3[]=$empleado->rfc;
            $data4[]=$empleado->nss;
            }
            $total=count($data1);
            $data[]=array('nombres'=>$data1,'curp'=>$data2,'rfc'=>$data3,'nss'=>$data4,'total'=>$total,'empresa'=>$empresa);
           return response()->json($data);
    }   
    public function getGeneracion()
    {
         /*   select empresaremun_id, COUNT(CASE WHEN generacion= 'afore' THEN 1 END) as afore,
            COUNT(CASE WHEN generacion= 'transicion' THEN 1 END) AS Transicion from  empleados group by empresaremun_id
*/
                $generacion= DB::select( DB::raw("select empresaremuns.id,rfc_em_l,num_registro_p,representante_l,razon_social, COUNT(CASE WHEN generacion= 'afore' THEN 1 END) as afore,
                                                  COUNT(CASE WHEN generacion= 'transicion' THEN 1 END) AS transicion from  empleados  inner join empresaremuns on empleados.empresaremun_id=empresaremuns.id group by empresaremun_id"));
            return view('empresasRemuneradoras.generacion',['generacion'=>$generacion]);
    }
     public function postGeneracion()
    {
            $id = Input::get('d');
            $tipo_generacion = Input::get('e');   
            if($tipo_generacion == 1)
            $tipo_generacion = "afore";
            if($tipo_generacion ==2)
            $tipo_generacion= "transicion";        
            $data =array();
            $data1 =array();
            $data2 = array();
             $empresa = empresaremun::select('razon_social')
                                             ->where('id',$id)
                                             ->get();
            foreach (empleados::where('empresaremun_id',$id)->where('generacion',$tipo_generacion)->get() as $empleado) {
            $data1[]=$empleado->nombres." ".$empleado->apellidoP." ".$empleado->apellidoM;
            $data2[]=$empleado->curp;
            $data3[]=$empleado->rfc;
            $data4[]=$empleado->nss;
        }
            $total=count($data1);
            $data[]=array('nombres'=>$data1,'curp'=>$data2,'rfc'=>$data3,'nss'=>$data4,'total'=>$total,'empresa'=>$empresa,'generacion'=>$tipo_generacion);
           return response()->json($data);
       }   

}


  /*        $seguimiento = DB::select( DB::raw("select nombres,salarioppp,comision,iva,total1,contingencia,ganancia,total2,
                                                ((total2)*30)/100 as vendedores,((total2)*70)/100 as ciss
                                                from(select nombres,salarioppp,comision,iva,total1,contingencia,ganancia,
                                                ganancia as total2
                                                from(select nombres,salarioppp,comision,iva,total1,((total1)*20)/100 as contingencia,
                                                ((total1)*80)/100 as ganancia
                                                from(select nombres,salarioppp,comision,iva,(comision + iva) as total1
                                                from(select nombres,salarioppp,((salarioPPP)*8)/100 as comision,
                                                ((salarioPPP)*16)/100 as iva  from empleados)as sub1)as sub2)as sub3)as sub4"));
            return view('empresasRemuneradoras.seguimiento',['seguimiento'=>$seguimiento]);
  







*/