<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class empresas extends Model {

	public $timestamps=false;

	protected $fillable = ['razon_social','domicilio','rfc_em_l','representante_l','num_contacto','nombre_contacto','num_registro_p',
	'acta_constitutiva','poder_notarial','ife_representante_l','satic','psi'];
	
	public function empleados(){
		return $this->hasMany('App\empleados');
	}

}