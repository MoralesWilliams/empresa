<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class empleados extends Model {

		public $timestamps=false;

	protected $fillable = ['empresaremun_id','nombres','apellidoP','apellidoM','empresas_id','curp','edad','generacion','rfc','nss','ife',
							'fecha_nac','telefono','salario','tipo_salario','tipo_sangre','donador_organos','salarioPPP','tipo_trabajador','contrato_individual','acta_nac','comprobante_dom','solicitud_pensiones','fonacot'];

	public function empresas(){
		return $this->belongsTo('App\empresas','empresas_id');
	}
	public function empresasremun(){
		return $this->belongsTo('App\empresaremun','empresaremun_id');
	}
}
